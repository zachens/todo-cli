# rust todo-cli

This project is personal, and made so I can further learn rust.  

## Dmenu Script 

I created a simple dmenu bash script, so that I can run this program via dmenu.  

In order to run the script, you have to build this project, and move the binary  
to /usr/bin/. or a custom location. As long as it is added to your PATH, and can  
be executed freely.

- [todo-cli dmenu script](https://gitlab.com/zachens/dotfiles/-/tree/master/.local/bin/todoDmenu) 

