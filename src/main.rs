use serde::{Deserialize, Serialize};
use std::{
    env,
    fs::{self, File},
    io::{BufWriter, Write},
    path::PathBuf,
};
use xdg::BaseDirectories;

#[derive(Debug, Serialize, Deserialize)]
struct TodoItem {
    _index: usize,
    _desc: String,
}

impl TodoItem {
    fn new(index: usize, desc: String) -> Self {
        Self {
            _index: index,
            _desc: desc,
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("Usage: {} <command> [arguments]", args[0]);
        return;
    }

    let cmd = args[1].to_lowercase();

    let xdg_dirs = BaseDirectories::with_prefix("todo-cli").unwrap();
    let file_path = xdg_dirs.place_config_file("todo.txt").unwrap();

    let mut todo_list: Vec<TodoItem> = read_file(&file_path);

    match cmd.as_str() {
        "add" => {
            if args.len() < 3 {
                println!("Usage: {} add <description>", args[0]);
                return;
            }
            let desc = args[2..].join(" ");
            let new_index = todo_list.len() + 1;
            let todo_item = TodoItem::new(new_index, desc);
            todo_list.push(todo_item);
            write_to_file(&file_path, &todo_list);
        }
        "rm" => {
            if args.len() < 3 {
                println!("Usage: {} rm <index>", args[0]);
                return;
            }
            if let Ok(index) = args[2].parse::<usize>() {
                if index <= todo_list.len() {
                    todo_list.remove(index - 1);
                    update_indices(&mut todo_list);
                    write_to_file(&file_path, &todo_list);
                } else {
                    println!("Index out of range.");
                }
            } else {
                println!("Invalid index.");
            }
        }
        "list" => {
            for item in todo_list {
                println!("{:?}: {:?}", item._index, item._desc);
            }
        }
        _ => println!("Invalid command"),
    }
}

fn read_file(file_path: &PathBuf) -> Vec<TodoItem> {
    if let Ok(contents) = fs::read_to_string(file_path) {
        serde_json::from_str(&contents).unwrap_or_default()
    } else {
        Vec::new()
    }
}

fn write_to_file(file_path: &PathBuf, todo_list: &Vec<TodoItem>) {
    if let Ok(file) = File::create(file_path) {
        let mut writer = BufWriter::new(file);
        let serialized = serde_json::to_string_pretty(todo_list).unwrap();
        writer.write_all(serialized.as_bytes()).unwrap();
    }
}

fn update_indices(todo_list: &mut Vec<TodoItem>) {
    for (index, todo) in todo_list.iter_mut().enumerate() {
        todo._index = index + 1;
    }
}
